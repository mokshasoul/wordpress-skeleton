<?php
init_set('display_errors', 0);
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */
if ( file_exists( dirname(__FILE__) . '/../production-config.php' ) ) {
    define( 'WP_LOCAL_DEV', false);
    include( dirname(__FILE__) . '/../production-config.php');
}else{
    define('WP_LOCAL_DEV', true );
    include( dirname(__FILE__) . '/../local-config.php');
}

//================================================================
// Point to worpdress root, home is current root
//================================================================
define ('WP_SITEURL', 'http://'. $_SERVER['SERVER_NAME'] . '/wordpress');
define('WP_HOME',    'http://' . $_SERVER['SERVER_NAME']);

// ================
// Custom content directory
// ================
define( 'WP_CONTENT_DIR', dirname(__FILE__) . '/wp-content');
define( 'WP_CONTENT_URL', 'http://' . $_SERVER['DOCUMENT_ROOT'] . '/wp-content');

/** Database Charset to use in creating database tables. */

define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);
define('WP_DEBUG_DISPLAY', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') ){
    define('ABSPATH', dirname(__FILE__) . '/wordpress/');
}
/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
